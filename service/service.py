# coding=utf-8
from main.settings import SCHEDULE_SERVER, MAIN_SERVER, ACCOUNTS_SERVER
from django.utils.crypto import get_random_string as rs
__author__ = 'veesot'


def get_random_string(n: int = 10) -> str:
    return rs(n)


# Взято с https://djangosnippets.org/snippets/1474/
def get_referer_view(request, default=None):
    """
    Return the referer view of the current request

    Example:

        def some_view(request):
            ...
            referer_view = get_referer_view(request)
            return HttpResponseRedirect(referer_view, '/accounts/login/')
    """

    # if the user typed the url directly in the browser's address bar
    refer = request.META.get('HTTP_REFERER')
    if not refer:
        return default


def get_sub_domain():
    sub_domains = {
        'accounts': ACCOUNTS_SERVER,
        'schedule': SCHEDULE_SERVER,
        'default': MAIN_SERVER
    }
    return sub_domains


def get_redirect_page(redirect):
    sub_domains = get_sub_domain()
    if redirect in sub_domains:
        return sub_domains[redirect]
    else:
        return sub_domains['default']
