pytest
requests
django == 1.8.1
mysqlclient
gunicorn
django-bootstrap3
djangorestframework
django-filter