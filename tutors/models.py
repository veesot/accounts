from django.contrib.auth.models import Group, UserManager
from django.db.models import CharField

from accounts.models import Account


class Tutor(Account):
    objects = UserManager()
    disciplines = CharField(max_length=30, blank=True)

    class Meta:
        db_table = 'tutors'
        verbose_name = 'Преподователи'
        verbose_name_plural = "Пользователи"

    def save(self, *args, **kwargs):
        # if user hasnt ID - is creationg operation
        created = self.id is None
        super().save(*args, **kwargs)

        # after save user has ID
        # add user to group only after creating
        if created:
            g = Group.objects.get(name="tutors")
            g.user_set.add(self.id)

    def get_short_name(self):
        super().get_short_name()

    def get_full_name(self):
        super().get_full_name()
