# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tutor',
            fields=[
                ('account_ptr', models.OneToOneField(serialize=False, parent_link=True, auto_created=True, to='accounts.Account', primary_key=True)),
                ('disciplines', models.CharField(blank=True, max_length=30)),
            ],
            options={
                'verbose_name_plural': 'Пользователи',
                'verbose_name': 'Преподователи',
                'db_table': 'tutors',
            },
            bases=('accounts.account',),
        ),
    ]
