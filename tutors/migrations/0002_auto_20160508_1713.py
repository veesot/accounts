# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-08 12:13
from __future__ import unicode_literals

import django.contrib.auth.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tutors', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='tutor',
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
