import json

import requests
from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin, \
    DestroyModelMixin, ListModelMixin, CreateModelMixin
from rest_framework.serializers import HyperlinkedModelSerializer

from api.models import JSONResponse, GroupSerializer
from main.settings import SCHEDULE_SERVER
from tutors.models import Tutor


# class TutorFilter(django_filters.FilterSet):
#     first_name = django_filters.CharFilter(name="first_name", lookup_type='exact')
#     last_name = django_filters.CharFilter(name="last_name", lookup_type='exact')
#     middle_name = django_filters.CharFilter(name="middle_name", lookup_type='exact')
#
#     class Meta:
#         model = Tutor
#         fields = ['first_name', 'last_name', 'middle_name']
#
#
class TutorSerializer(HyperlinkedModelSerializer):
    disciplines = serializers.ListField(
        child=serializers.CharField()
    )
    groups = GroupSerializer(many=True)

    class Meta:
        model = Tutor
        fields = ('username', 'first_name', 'last_name', 'middle_name', 'id', 'disciplines', 'groups', 'email')


class TutorList(ListModelMixin, CreateModelMixin, GenericAPIView):
    queryset = Tutor.objects.all()
    serializer_class = TutorSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def create(self, validated_data, **kwargs):
        tutor = Tutor.objects.create(**validated_data.data)
        g = Group.objects.get(name='tutors')
        g.user_set.add(tutor.id)
        return tutor

    def post(self, request, *args, **kwargs):
        tutor = self.create(request)
        serializer = TutorSerializer(tutor)
        return JSONResponse(serializer.data, status=201)


class TutorDetail(RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin, GenericAPIView):
    queryset = Tutor.objects.all()
    serializer_class = TutorSerializer

    # GET

    def get(self, request, *args, **kwargs):
        tutor = self.retrieve(request, *args, **kwargs)
        serializer = TutorSerializer(tutor)
        return JSONResponse(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        idx = int(kwargs.get('pk'))
        tutor = Tutor.objects.get(id=idx)
        disciplines = request.GET.get('disciplines', None)

        if disciplines:  # Нужно приложить информацию о преподаваемых дисциплинах
            disciplines = set()
            r = requests.get(SCHEDULE_SERVER + '/api/v1/timetables/')
            all_timetables = json.loads(r.content.decode())
            for timetable in all_timetables:
                # Вытащим название расписания
                timetable_title = timetable.get('title')
                # После чего вытащим дисциплины по этому расписанию с привязкой к преподователям
                url = SCHEDULE_SERVER + '/api/v1/disciplines/'
                params = {'timetable': timetable_title, 'tutors': 'true'}
                r = requests.get(url, params=params)
                all_disciplines = json.loads(r.content.decode())
                for discipline in all_disciplines:
                    tutor_ids = [tutor.get('id') for tutor in discipline.get('tutors')]  # Идентификаторы преподователей
                    if idx in tutor_ids:  # Ищем среди тех кто преподает даную дисциплину
                        disciplines.add(discipline.get('title'))
            tutor.disciplines = list(disciplines)
        return tutor

        # PUT

    def update(self, request, *args, **kwargs):
        tutor = Tutor.objects.get(id=kwargs.get('pk'))
        public_props = ['username', 'first_name', 'last_name', 'middle_name']

        for name in public_props:
            setattr(tutor, name, request.data.pop(name))

        for group in tutor.groups.all():
            tutor.groups.remove(group)

        groups = request.data.get('groups')
        for group in groups:
            tutor.groups.add(Group.objects.get(id=group['id']))

        tutor.save()

        return tutor

    def put(self, request, *args, **kwargs):
        tutor = self.update(request, *args, **kwargs)
        serializer = TutorSerializer(tutor)
        return JSONResponse(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        tutor = Tutor.objects.get(id=kwargs.get('pk'))
        update_fields = request.data.keys()
        for update_field in update_fields:
            setattr(tutor, update_field, request.data.get(update_field))

        tutor.save()

        return tutor

    def patch(self, request, *args, **kwargs):
        tutor = self.partial_update(request, *args, **kwargs)
        serializer = TutorSerializer(tutor)
        # Т.к. возможно запросили изменение атрибутов не входящих в стандартную компектацию -
        # стоит отобразить их тоже
        for key in request.data.keys():
            serializer.data[key] = getattr(tutor, key)
        return JSONResponse(serializer.data)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
