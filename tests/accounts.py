import json
import random

import pytest
from accounts.models import Account
from rest_framework import status
from rest_framework.test import APITestCase
from tests.common_services import CommonTestMethods

@pytest.mark.django_db
class AccountTestCase(APITestCase):
    base_url = '/api/accounts/'

    def setUp(self):
        self.user = CommonTestMethods.create_superuser()
        self.client = CommonTestMethods.get_client_with_csrf_token()
        self.groups = CommonTestMethods.create_groups(['students', 'tutors'])

    def test_create_user(self):
        data = {"username": "TestUser", "groups": [random.choice(self.groups)]}
        response = self.client.post(AccountTestCase.base_url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Account.objects.count(), 2)  # We also have superuser
        self.assertEqual(Account.objects.last().username, 'TestUser')

    def test_get_user(self):
        idx = Account.objects.first().id
        url = AccountTestCase.base_url + '{idx}/'.format(idx=idx)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Account.objects.last().username, 'user')

    def test_user_list(self):
        response = self.client.get(AccountTestCase.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_full_update_user(self):
        """Full update info about superuser"""
        data = {"username": "TestUser",
                'first_name': 'User',
                'last_name': 'Test',
                'middle_name': 'TestUser',
                "groups": [random.choice(self.groups)]}
        idx = Account.objects.first().id
        url = AccountTestCase.base_url + '{idx}/'.format(idx=idx)
        response = self.client.put(url, json.dumps(data), content_type='application/json')
        json_response = json.loads(response.content.decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Account.objects.count(), 1)  # Not new users
        self.assertEqual(idx, json_response.pop('id'))
        self.assertEqual(json_response, data)

    def test_partly_update_user(self):
        """Partly update info about superuser"""
        data = {"groups": [random.choice(self.groups)], 'email': 'yyy@yyy.yyy'}
        idx = Account.objects.first().id
        url = AccountTestCase.base_url + '{idx}/'.format(idx=idx)
        response = self.client.patch(url, json.dumps(data), content_type='application/json')
        json_response = json.loads(response.content.decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(idx, json_response.pop('id'))
        self.assertEqual(data.pop('groups'), json_response.pop('groups'))

    def test_delete_user(self):
        data = {"username": "TestUser", "groups": [random.choice(self.groups)]}
        response = self.client.post(AccountTestCase.base_url, json.dumps(data), content_type='application/json')
        json_response = json.loads(response.content.decode())
        idx = json_response.pop('id')
        url = AccountTestCase.base_url + '{idx}/'.format(idx=idx)
        response = self.client.delete(url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Account.objects.count(), 1)  # Only superuser
