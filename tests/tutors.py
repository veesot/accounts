import json

from rest_framework import status
from rest_framework.test import APITestCase

from tests.common_services import CommonTestMethods
from tutors.models import Tutor


class TutorTestCase(APITestCase):
    base_url = '/api/tutors/'

    def setUp(self):
        self.user = CommonTestMethods.create_superuser()
        self.client = CommonTestMethods.get_client_with_csrf_token()
        self.groups = CommonTestMethods.create_groups(['tutors'])

    def test_create_tutor(self):
        data = {"username": "TestUser"}
        response = self.client.post(TutorTestCase.base_url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Tutor.objects.count(), 1)  # Only one tutor

    def test_get_tutor(self):
        tutor = Tutor.objects.create(username="TestUser")
        idx = tutor.id
        url = TutorTestCase.base_url + '{idx}/'.format(idx=idx)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = json.loads(response.content.decode())  # JSON-rerp response

        self.assertEqual(json_response.pop('username'), 'TestUser', "Username not correct")

        groups = [group.get('name') for group in json_response.get('groups')]
        self.assertTrue('tutors' in groups, "User not added in tutor group")

    def test_tutor_list(self):
        Tutor.objects.create(username="TestUser1")
        Tutor.objects.create(username="TestUser2")
        response = self.client.get(TutorTestCase.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_response = json.loads(response.content.decode())
        self.assertEqual(len(json_response), 2)  # We get 2 users

    def test_full_update_tutor(self):
        """Full update info about tutor"""
        origin_tutor = Tutor.objects.create(username="OriginTutor")
        data = {"username": "TestUser",
                'first_name': 'User',
                'last_name': 'Test',
                'middle_name': 'TestUser',
                "groups": []}
        idx = origin_tutor.id
        url = TutorTestCase.base_url + '{idx}/'.format(idx=idx)
        response = self.client.put(url, json.dumps(data), content_type='application/json')
        json_response = json.loads(response.content.decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Tutor.objects.count(), 1)  # Not new users
        self.assertEqual(idx, json_response.pop('id'))
        self.assertEqual(json_response.get('username'), data.get('username'))
        self.assertEqual(json_response.get('first_name'), data.get('first_name'))
        self.assertEqual(json_response.get('last_name'), data.get('last_name'))
        self.assertEqual(json_response.get('middle_name'), data.get('middle_name'))

    def test_partly_update_tutor(self):
        """Partly update info about tutor"""
        origin_tutor = Tutor.objects.create(username="OriginTutor")
        idx = origin_tutor.id
        data = {'email': 'yyy@yyy.yyy'}
        url = TutorTestCase.base_url + '{idx}/'.format(idx=idx)
        response = self.client.patch(url, json.dumps(data), content_type='application/json')
        json_response = json.loads(response.content.decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Tutor.objects.count(), 1)
        self.assertEqual(idx, json_response.pop('id'))
        self.assertEqual(data.pop('email'), json_response.pop('email'))

    def test_delete_tutor(self):
        origin_tutor = Tutor.objects.create(username="OriginTutor")
        idx = origin_tutor.id
        url = TutorTestCase.base_url + '{idx}/'.format(idx=idx)
        response = self.client.delete(url, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Tutor.objects.count(), 0, "Tutor not delete")
