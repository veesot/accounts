from typing import List

import pytest
from accounts.models import Account
from django.contrib.auth.models import Group
from rest_framework.test import APIClient

@pytest.mark.django_db
class CommonTestMethods:
    @classmethod
    def get_client_with_csrf_token(cls) -> APIClient:
        client = APIClient(enforce_csrf_checks=True)
        client.login(username='user', password='pass')
        resp = client.get('/profile/')
        client.credentials(HTTP_X_CSRFTOKEN=resp.cookies['csrftoken'].value)
        return client

    @classmethod
    def create_groups(cls, name_list: List[str]):
        group_list = []
        for name in name_list:
            group_list.append(Group.objects.create(name=name))
        group_list = [{'name': group.name, 'id': group.id} for group in group_list]
        return group_list

    @classmethod
    def create_superuser(cls):
        return Account.objects.create_superuser(email="xxx@xxx.xxx", password='pass', username="user")
