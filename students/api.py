from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin, \
    DestroyModelMixin, ListModelMixin, CreateModelMixin
from rest_framework.serializers import HyperlinkedModelSerializer
from api.models import JSONResponse, StudyGroupSerializer
from students.models import Student


class StudentSerializer(HyperlinkedModelSerializer):
    study_group = StudyGroupSerializer()

    class Meta:
        model = Student
        fields = ('study_group',)


class StudentList(ListModelMixin, CreateModelMixin, GenericAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def create(self, validated_data, **kwargs):
        student = Student.objects.create(**validated_data.data)
        g = Group.objects.get(name='students')
        g.user_set.add(student.id)
        return student

    def post(self, request, *args, **kwargs):
        student = self.create(request)
        serializer = StudentSerializer(student)
        return JSONResponse(serializer.data, status=201)


class StudentDetail(RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin, GenericAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

    # GET

    def get(self, request, *args, **kwargs):
        student = self.retrieve(request, *args, **kwargs)
        serializer = StudentSerializer(student,context={'request': request})
        return JSONResponse(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        idx = int(kwargs.get('pk'))
        student = Student.objects.get(id=idx)
        return student

        # PUT

    def update(self, request, *args, **kwargs):
        student = Student.objects.get(id=kwargs.get('pk'))
        public_props = ['username', 'first_name', 'last_name', 'middle_name']

        for name in public_props:
            setattr(student, name, request.data.pop(name))

        for group in student.groups.all():
            student.groups.remove(group)

        groups = request.data.get('groups')
        for group in groups:
            student.groups.add(Group.objects.get(id=group['id']))

        student.save()

        return student

    def put(self, request, *args, **kwargs):
        student = self.update(request, *args, **kwargs)
        serializer = StudentSerializer(student)
        return JSONResponse(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        student = Student.objects.get(id=kwargs.get('pk'))
        update_fields = request.data.keys()
        for update_field in update_fields:
            setattr(student, update_field, request.data.get(update_field))

        student.save()

        return student

    def patch(self, request, *args, **kwargs):
        student = self.partial_update(request, *args, **kwargs)
        serializer = StudentSerializer(student)
        # Т.к. возможно запросили изменение атрибутов не входящих в стандартную компектацию -
        # стоит отобразить их тоже
        for key in request.data.keys():
            serializer.data[key] = getattr(student, key)
        return JSONResponse(serializer.data)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
