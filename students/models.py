from django.contrib.auth.models import Group, UserManager
from django.db.models import CharField, Model, ForeignKey, BooleanField
from accounts.models import Account


class StudyGroup(Model):
    title = CharField(verbose_name="Название студенческой группы", max_length=10)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'study_groups'
        verbose_name = 'Студенческая группа'
        verbose_name_plural = "Студенческие группы"


class Student(Account):
    objects = UserManager()
    learn_language = CharField(max_length=10, blank=True, verbose_name="Изучаемый язык")
    liberation = BooleanField(default=False, verbose_name="Освобождение от физкультуры")
    orphan = BooleanField(default=False, verbose_name="Сирота")
    study_group = ForeignKey(StudyGroup, blank=True)

    class Meta:
        db_table = 'students'
        verbose_name = 'Студент'
        verbose_name_plural = "Студенты"

    def save(self, *args, **kwargs):
        # if user hasnt ID - is creationg operation
        created = self.id is None
        super().save(*args, **kwargs)

        # after save user has ID
        # add user to group only after creating
        if created:
            g = Group.objects.get(name="students")
            g.user_set.add(self.id)

    def get_short_name(self):
        super().get_short_name()

    def get_full_name(self):
        super().get_full_name()
