# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import socket

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

PROJECT_ROOT = os.path.realpath(os.path.dirname(__file__))

CUSTOM_USER_MODEL = 'accounts.Account'

AUTHENTICATION_BACKENDS = (
    'accounts.auth_backends.CustomUserModelBackend',
    'django.contrib.auth.backends.ModelBackend',
)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY', 'super_secret_key')

ALLOWED_HOSTS = [os.getenv('ALLOWED_HOST', 'localhost')]

# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    'accounts',
    'tutors',
    'students',
    'bootstrap3',
    'rest_framework',
    'rest_framework.authtoken',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication'
    ),
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

ROOT_URLCONF = 'main.urls'
WSGI_APPLICATION = 'main.wsgi.application'
TEMPLATE_DIRS = ('templates',)
STATIC_URL = '/assets/'
STATIC_ROOT = BASE_DIR + STATIC_URL

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

DEBUG = True
MAIN_SERVER = os.getenv('MAIN_SERVER', 'http://localhost:8000/')
ACCOUNTS_SERVER = os.getenv('ACCOUNTS_SERVER', 'http://localhost:8001')
SCHEDULE_SERVER = os.getenv('SCHEDULE_SERVER', 'http://localhost:8002')
DB_HOST = os.getenv('DB_HOST', 'localhost')
TEMPLATE_DEBUG = True
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'spark',
        'USER': 'root',
        'PASSWORD': "toor",
        'HOST': DB_HOST,  # Set to empty string for localhost.
        'PORT': '',  # Set to empty string for default.
        'TEST': {
            'NAME': 'test',
        },
    },

}
TIME_ZONE = 'Asia/Yekaterinburg'
USE_TZ = True

LANGUAGE_CODE = 'ru-RU'

SHORT_DATE_FORMAT = 'd E'
TIME_FORMAT = 'H:i'

LANGUAGES = (
    ('ru', 'Russian'),
    ('en', 'English'),
)
USE_I18N = True
USE_L10N = False
