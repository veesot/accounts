# -*- coding: utf-8 -*-

from django.shortcuts import redirect as redirect_to_page

from service.service import get_redirect_page


def index(request):
    if request.user.id:  # Если это авторизованый пользователь
        # Покажем ему его же профиль
        response = redirect_to_page('/users/{0}'.format(request.user.id))
    else:
        response = redirect_to_page('/login/')

    return response
