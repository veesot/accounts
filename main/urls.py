from django.conf.urls import url, include
from django.contrib import admin

from api.models import AccountList, AccountDetail, GroupList, GroupDetail, GroupAccountsList
from students.api import StudentDetail, StudentList
from tutors.api import TutorDetail, TutorList

urlpatterns = [

    url(r'^api/accounts/$', AccountList.as_view()),
    url(r'^api/accounts/(?P<pk>[0-9]+)/$', AccountDetail.as_view()),

    url(r'^api/tutors/$', TutorList.as_view()),
    url(r'^api/tutors/(?P<pk>[0-9]+)/$', TutorDetail.as_view()),

    url(r'^api/students/(?P<pk>[0-9]+)/$', StudentDetail.as_view()),

    url(r'^api/groups/$', GroupList.as_view()),
    url(r'^api/groups/(?P<pk>[0-9]+)/$', GroupDetail.as_view()),
    url(r'^api/groups/(?P<pk>[0-9]+)/accounts/$', GroupAccountsList.as_view()),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^panel_control/', include(admin.site.urls)),
    url(r'^', include('accounts.urls')),
    url(r'^api/', include('api.urls')),
]
