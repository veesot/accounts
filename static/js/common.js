function changeGroupVisibility(obj) {
    var selectBox = obj;
    var selected = selectBox.options[selectBox.selectedIndex].value;
    var study_group = document.getElementById("div_study_group");
    var orphan = document.getElementById("div_orphan");
    var registration = document.getElementById("div_registration");
    var passport_number = document.getElementById("div_passport_number");
    var learn_language = document.getElementById("div_learn_language");
    var liberation = document.getElementById("div_liberation");

    if (selected === 'students') {
        study_group.style.display = "block";
        orphan.style.display = "block";
        registration.style.display = "block";
        passport_number.style.display = "block";
        learn_language.style.display = "block";
        liberation.style.display = "block";
    }
    else {
        study_group.style.display = "none";
        orphan.style.display = "none";
        registration.style.display = "none";
        passport_number.style.display = "none";
        learn_language.style.display = "none";
        liberation.style.display = "none";
    }
}