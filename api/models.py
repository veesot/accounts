import inspect
import json
from typing import List
import django_filters
from accounts.models import Account
from django.contrib.auth.models import Group
from django.contrib.sessions.models import Session
from django.http import HttpResponse
from rest_framework import filters
from rest_framework.fields import CharField, IntegerField
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin, RetrieveModelMixin, CreateModelMixin, \
    ListModelMixin
from rest_framework.renderers import JSONRenderer
from rest_framework.serializers import HyperlinkedModelSerializer

# New API
from students.models import StudyGroup


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


class StudyGroupSerializer(HyperlinkedModelSerializer):
    title = CharField()

    class Meta:
        model = StudyGroup
        exclude = ('url',)


class StudyGroupDetail(RetrieveModelMixin, GenericAPIView):
    queryset = StudyGroup.objects.all()
    serializer_class = StudyGroupSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class GroupSerializer(HyperlinkedModelSerializer):
    name = CharField(max_length=200)
    id = IntegerField()

    class Meta:
        model = Group
        exclude = ('url', 'permissions')


class GroupList(ListModelMixin, CreateModelMixin, GenericAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class GroupDetail(RetrieveModelMixin, GenericAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class AccountFilter(django_filters.FilterSet):
    first_name = django_filters.CharFilter(name="first_name", lookup_type='exact')
    last_name = django_filters.CharFilter(name="last_name", lookup_type='exact')
    middle_name = django_filters.CharFilter(name="middle_name", lookup_type='exact')

    class Meta:
        model = Account
        fields = ['first_name', 'last_name', 'middle_name']


class AccountSerializer(HyperlinkedModelSerializer):
    groups = GroupSerializer(many=True)

    class Meta:
        model = Account
        fields = ('username', 'first_name', 'last_name', 'middle_name', 'id', 'groups')


class AccountList(ListModelMixin, CreateModelMixin, GenericAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = AccountFilter

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def create(self, validated_data, **kwargs):
        groups = validated_data.data.pop('groups', [])
        included_groups = []
        for group in groups:
            included_groups.append(Group.objects.get(id=group['id']))
        user = Account.objects.create(**validated_data.data)
        for included_group in included_groups:
            user.groups.add(included_group)
        return user

    def post(self, request, *args, **kwargs):
        user = self.create(request)
        serializer = AccountSerializer(user)
        return JSONResponse(serializer.data, status=201)


class AccountDetail(RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin, GenericAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        account = Account.objects.get(id=kwargs.get('pk'))
        public_props = ['username', 'first_name', 'last_name', 'middle_name']

        for name in public_props:
            setattr(account, name, request.data.pop(name))

        for group in account.groups.all():
            account.groups.remove(group)

        groups = request.data.get('groups')
        for group in groups:
            account.groups.add(Group.objects.get(id=group['id']))

        account.save()

        return account

    def put(self, request, *args, **kwargs):
        account = self.update(request, *args, **kwargs)
        serializer = AccountSerializer(account)
        return JSONResponse(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        account = Account.objects.get(id=kwargs.get('pk'))
        groups = request.data.pop('groups', None)
        if groups:
            # Обновляем список групп,если кто то решил их изменить
            for group in account.groups.all():
                account.groups.remove(group)
            for group in groups:
                account.groups.add(Group.objects.get(id=group['id']))

        update_fields = request.data.keys()
        for update_field in update_fields:
            setattr(account, update_field, request.data.get(update_field))

        account.save()

        return account

    def patch(self, request, *args, **kwargs):
        account = self.partial_update(request, *args, **kwargs)
        serializer = AccountSerializer(account)
        # Т.к. возможно запросили изменение атрибутов не входящих в стандартную компектацию -
        # стоит отобразить их тоже
        for key in request.data.keys():
            serializer.data[key] = getattr(account, key)
        return JSONResponse(serializer.data)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class GroupAccountsList(ListModelMixin, GenericAPIView):
    queryset = Account.objects.all()

    def get(self, request, *args, **kwargs):
        accounts = self.list(request, *args, **kwargs)
        response = [AccountSerializer(account).data for account in accounts]
        return JSONResponse(response)

    def list(self, request, *args, **kwargs) -> List[Account]:
        group_id = kwargs.get('pk')
        # Отбираем только аккаунты входящие в указаную группу
        accounts = Account.objects.filter(groups__id=group_id)
        return accounts


class Api:
    """The interaction with other parts application."""

    def __init__(self):
        pass

    @classmethod
    def session_exist(cls, request, cookie_id: str) -> HttpResponse:
        """
        Return session id if session exist in db else return None.
        Example request:/api/v1/session_exist/session_id
        :param cookie_id: Cookie for search in DB
        """
        existence_session = Session.objects.filter(session_key=cookie_id).exists()
        response_data = {'session_exists': existence_session}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    @classmethod
    def get_user_id(cls, request, session_key: str) -> HttpResponse:
        """
        Return user id if user exist else return None.
        Example request:/api/v1/get_user_id/session_key
        :param session_key: Key based on cookie
        """
        session = Session.objects.get(session_key=session_key)
        session_data = session.get_decoded()
        user_id = session_data.get('_auth_user_id')
        response_data = {'user_id': int(user_id)}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    @classmethod
    def get_user(cls, request, user_id: int) -> HttpResponse:
        """
        Return information about user.
        Example request:/api/v1/user/user_id
        :param user_id Identification for Account
        """
        default_attributes = ['email', 'username', 'is_superuser', 'first_name', 'last_name']
        user = Account.objects.get(id=user_id)
        response = {attribute: getattr(user, attribute) for attribute in default_attributes}
        return HttpResponse(json.dumps(response), content_type="application/json")

    @staticmethod
    def api_list(request):
        methods = inspect.getmembers(Api, predicate=inspect.ismethod)
        response_data = {}
        for method in methods:
            doc = method[1].__doc__
            doc = doc.replace("\n", "").replace('        ', '')
            response_data[method[0]] = doc

        return HttpResponse(json.dumps(response_data), content_type="application/json")
