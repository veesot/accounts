from django.conf.urls import url

from .models import Api

urlpatterns = [
    url(r'^v1/$', Api.api_list, name='api_list'),
    url(r'^v1/session_exist/(?P<cookie_id>[\a-z\d+]+)$', Api.session_exist, name='session_exist'),
    url(r'^v1/get_user_id/(?P<session_key>[\a-z\d+]+)$', Api.get_user_id, name='get_user_id'),
    url(r'^v1/get_user/(?P<user_id>[\d+]+)$', Api.get_user, name='get_user')
]
