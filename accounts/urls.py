from django.conf.urls import patterns, url

from main.views import index
from .views import vk_login, ok_login, logout_user, users_list, profile,login

urlpatterns = patterns('',
                       url(r'^$', index, name='index'),
                       url(r'^login/$', login, name='login'),
                       url(r'^profile/$', profile, name='home'),
                       url(r'^login_vk/$', vk_login, name='vkontakte'),
                       url(r'^login_ok/$', ok_login, name='odnoklassniki'),
                       url(r'^log_out/$', logout_user, name='logout_user'),
                       url(r'^users/$', users_list, name='accounts'),
                       url(r'^users/(?P<pk>[0-9]+)/$', users_list, name='accounts'),
                       )
