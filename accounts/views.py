# coding=utf-8
import json
import requests
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import login as login_to_system
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import redirect, render
from accounts.forms import AccountForm
from main.settings import ACCOUNTS_SERVER
from students.models import Student, StudyGroup
from tutors.models import Tutor
from .models import Account
from .social import Ok, Vk


def ok_login(request):
    # Начнем общение с Одноклассниками
    ok = Ok()
    return ok.login(request)


def vk_login(request):
    vk = Vk()
    return vk.login(request)


def logout_user(request):
    Account.log_out(request)
    return redirect('/login')


def profile(request):
    # redirect_page = request.COOKIES.get('redirect_to')
    # if redirect_page:
    #     request.COOKIES.pop('redirect_to')
    #     response = redirect(redirect_page)
    #     return response
    account = request.user.account
    context = {}
    context.update({'account': account})
    return render(request, 'accounts/user.html', context)


def login(request):
    if request.method == "GET":
        form = AccountForm()
        return render(request, 'login.html', {'form': form})
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        if username.find("@") != -1:  # Это авторизация по электронной почте
            user = Account.objects.filter(email=username)
            if user:
                username = user[0].username  # Обновим значение имени пользователя
        user = authenticate(username=username, password=password)
        if user is not None:
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login_to_system(request, user)
        return redirect('/users/{0}'.format(request.user.id))


def users_list(request, pk=None):
    if request.user.id is not None:
        if request.method == "GET":
            if pk is None:  # Весь список пользовтаелей
                tutors = request.GET.get('tutors')
                students = request.GET.get('students')
                if tutors:
                    users = Tutor.objects.all()
                elif students:
                    users = Student.objects.all()
                else:
                    users = Account.objects.all()
                paginator = Paginator(users.order_by('-id'), 10)
                page = request.GET.get('page')
                try:
                    users = paginator.page(page)
                except PageNotAnInteger:
                    # If page is not an integer, deliver first page.
                    users = paginator.page(1)
                except EmptyPage:
                    # If page is out of range (e.g. 9999), deliver last page of results.
                    users = paginator.page(paginator.num_pages)
                context = {'users': users}
                if request.user.is_superuser:
                    study_groups = StudyGroup.objects.all()
                    context['study_groups'] = study_groups
                    form = AccountForm()
                    context.update({'form': form})
                return render(request, 'accounts/users.html', context)
            else:
                context = {}
                try:
                    account = Account.objects.get(id=pk)
                    # Мы показываем профиль только его владельцу или админу
                    if request.user.is_superuser or account == request.user.account:
                        context.update({'account': account})
                        if hasattr(account, 'tutor') and account.tutor:
                            r = requests.get(
                                '{ACCOUNTS_SERVER}/api/tutors/{pk}?disciplines=true'.format(
                                    ACCOUNTS_SERVER=ACCOUNTS_SERVER,
                                    pk=account.pk))
                            if r.status_code == 200:
                                json_response = json.loads(r.content.decode())
                                disciplines = json_response.get('disciplines')
                                context.update({'disciplines': disciplines})
                        if request.user.is_superuser:
                            study_groups = StudyGroup.objects.all()
                            context.update({'study_groups': study_groups})
                            form = AccountForm(initial={'first_name': account.first_name,
                                                        'last_name': account.last_name,
                                                        'middle_name': account.middle_name,
                                                        'email': account.email})
                            context.update({'form': form})
                        return render(request, 'accounts/user.html', context)
                except:
                    users = Account.objects.all()
                    paginator = Paginator(users.order_by('-id'), 10)
                    users = paginator.page(1)
                    context = {'users': users}
                    if request.user.is_superuser:
                        study_groups = StudyGroup.objects.all()
                        context['study_groups'] = study_groups
                        form = AccountForm()
                        context.update({'form': form})
                return render(request, 'accounts/users.html', context)
        elif request.method == "POST":
            first_name = request.POST.get('first_name')
            last_name = request.POST.get('last_name')
            email = request.POST.get('email')
            middle_name = request.POST.get('middle_name')
            password = request.POST.get('password')
            username = '{last_name} {first_name}'.format(first_name=first_name, last_name=last_name)
            params = {'first_name': first_name,
                      'last_name': last_name,
                      'middle_name': middle_name,
                      'email': email,
                      'username': username,
                      'password': password}

            type_account = request.POST.get('type_account')
            if pk:  # Нужно изменить данные
                account = Account.objects.get(id=int(pk))
                account.first_name = first_name
                account.last_name = last_name
                account.middle_name = middle_name
                account.username = username
                if password:
                    account.set_password(password)
                account.save()
                if hasattr(account, 'student') and account.student:  # Возможно изменение группы
                    student = account.student
                    study_group_title = request.POST.get('study_group')
                    if student.study_group.title != study_group_title:  # Требуется обновить группу
                        study_group = StudyGroup.objects.get(title=study_group_title)
                        student.study_group = study_group
                        student.save()
                elif not hasattr(account, 'student') and not hasattr(account, 'tutor'):
                    # Не я вляется ни преподователем,ни студентом
                    if type_account == "tutors":  # Нужен преподователь
                        user = Tutor()
                    elif type_account == "students":  # Переводим в студенты
                        study_group_title = request.POST.get('study_group')
                        study_group = StudyGroup.objects.get(title=study_group_title)
                        user = Student()
                        user.study_group = study_group
                    else:
                        raise Exception("Not set group")
                    user.account_ptr_id = account.id
                    user.username = username
                    user.first_name = first_name
                    user.last_name = last_name
                    user.middle_name = middle_name
                    user.save()
                messages.add_message(request, messages.SUCCESS, 'Информация была успешно обновлена')
                return redirect(request.path)
            else:  # Нужно создать нового пользователя
                if type_account == "tutors":  # Нужен преподователь
                    Tutor.objects.create_user(**params)
                    messages.add_message(request, messages.SUCCESS,
                                         'Преподователь {username} был успешно добавлен'.format(username=username))
                elif type_account == "students":  # Соответственно студент
                    study_group_title = request.POST.get('study_group')
                    study_group = StudyGroup.objects.get(title=study_group_title)
                    student = Student.objects.create_user(**params)
                    student.study_group = study_group
                    student.save()
                    messages.add_message(request, messages.SUCCESS,
                                         'Студент {username} был успешно добавлен'.format(username=username))
                else:
                    messages.add_message(request, messages.ERROR, 'Упс...что то пошло не так....')

                return redirect('accounts')
    else:
        return render(request, 'login.html')
