import datetime
import json
from hashlib import md5

from django.contrib.auth import login
from django.shortcuts import redirect
from requests import post, get

from accounts.models import SocialUser, Account
from main.settings import ACCOUNTS_SERVER
from service.service import get_random_string


class Ok:
    def __init__(self):
        self.app_id = '1109695488'
        self.public_key = 'CBANJFCDEBABABABA'
        self.secret_key = '7EDBEFC14ECA4B34E9CFB5ED'
        self.url = 'http://www.odnoklassniki.ru/oauth/authorize'
        self.token_uri = 'http://api.odnoklassniki.ru/oauth/token.do'
        self.api_gate = 'http://api.odnoklassniki.ru/fb.do'
        self.agent = 'ok'
        self.redirect_uri = ACCOUNTS_SERVER + '/login_ok/'

    @staticmethod
    def login(request):
        # Начнем общение с Одноклассниками
        ok = Ok()
        if 'code' not in request.GET:
            # Если нам ничего не пришло в гет-параметрах
            # отсылаем в Однокласники на запрос кода для операции
            return Social.redirect_to(ok)
        else:
            # Получили новоиспечного или зарегистрированого ранее юзера.
            user = Social.auth(ok, request.GET['code'])
            # Логин в лоб не пройдет,т.к нужен бэкенд получаемый при аутентифкации.
            # Укажем его  напрямую
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
            return redirect('home')


class Vk:
    def __init__(self):
        self.app_id = '5453465'
        self.secret_key = 'b93jQ8QHpxQeJRFlc2jb'
        self.url = 'http://oauth.vk.com/authorize'
        self.token_uri = 'https://oauth.vk.com/access_token'
        self.api_gate = 'https://api.vk.com/method/'
        self.agent = 'vk'
        self.redirect_uri = ACCOUNTS_SERVER + '/login_vk/'

    @staticmethod
    def login(request):
        # Начнем общение с Одноклассниками
        vk = Vk()
        if 'code' not in request.GET:
            # Если нам ничего не пришло в гет-параметрах
            # отсылаем в Однокласники на запрос кода для операции
            return Social.redirect_to(vk)
        else:
            # Получили новоиспечного или зарегистрированого ранее юзера.
            user = Social.auth(vk, request.GET['code'])
            # Логин в лоб не пройдет,т.к нужен бэкенд получаемый при аутентифкации.
            # Укажем его  напрямую
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
            return redirect('home')


class Social:
    @staticmethod
    def redirect_to(social):
        """
        Редирект из социалки обратно на нашу страничку.
        По умолчанию возвращаем
        :param social:
        """
        # Страничка в социалке на которую мы будем отправлять пользователя дабы он авторизовался
        uri = '{}?client_id={}&response_type=code&redirect_uri={}'.format(social.url, social.app_id,
                                                                          social.redirect_uri)
        return redirect(uri)

    @staticmethod
    def auth(social, code:str):
        """Отсылка к социалке для запроса прав"""
        redirect_uri = social.redirect_uri
        client_id = social.app_id
        client_secret = social.secret_key
        token_uri = social.token_uri

        # Запрос на токен
        request_params = {'code': code, 'redirect_uri': redirect_uri,
                          'client_id': client_id, 'client_secret': client_secret}

        # Дополнительные параметры для запроса
        if social.__class__ == Ok:  # Параметры для Ok
            request_params['grant_type'] = 'authorization_code'

        post_data = post(token_uri, request_params)  # Вытащим результат
        response_as_string = post_data.text

        # Респонс сервера - так или иначе - это строка,а работать с ней не так удобно как с словарем
        dict_response = json.loads(response_as_string)

        access_token = dict_response['access_token']

        # Теперь у нас есть токен и мы може начать запрашивать данные пользователя
        # Различные соцсети поддерживают различные типы запросов.
        if isinstance(social, Ok):  # Ok предпочитают подписаные запросы
            sign_secret = md5((access_token + social.secret_key).encode()).hexdigest()  # Секретная часть подписи
            format_response = 'json'
            method = 'users.getCurrentUser'
            string_request = "application_key={}format={}method={}{}". \
                format(social.public_key, format_response, method, sign_secret)
            sign = md5(string_request.encode()).hexdigest()
            # Запрос на данные пользователя
            url = "{}?method={}&access_token={}&application_key={}&format={}&sig={}". \
                format(social.api_gate, method, access_token, social.public_key, format_response, sign)
        elif isinstance(social, Vk):  # VK же предпочитает отсылку через https
            uid = dict_response['user_id']
            method = 'users.get'
            fields = ['uid', 'first', 'name', 'last_name', 'sex', 'bdate']
            url = "{}{}?uids={}&fields={}&access_token={}" \
                .format(social.api_gate, method, uid, ','.join(fields), access_token)
        else:
            # Эксепшен на случай если что то пошло не так
            raise RuntimeError

        response = get(url).text
        dict_response = json.loads(response)
        # Приведение к единому образу и порядку
        dict_response = Social.unification(social, dict_response)

        return Social.get_user(dict_response)

    @staticmethod
    def unification(social, dict_response):
        # Т.к и отдают они данные в разных форматах - нормализируем к одному виду
        if isinstance(social, Ok):
            pass
            # dict_response['city'] = dict_response['location']['city']

        elif isinstance(social, Vk):
            # Во первых у VK другие имена для полей
            dict_response = dict_response['response'][0]

            # Бывает не указывают дату рождения
            pre_birthday = dict_response.get('bdate', (1, 1, 1970))
            birthday = datetime.datetime(int(pre_birthday[2]), int(pre_birthday[1]), int(pre_birthday[0]))
            dict_response['birthday'] = birthday

            dict_response['name'] = dict_response['first_name'] + ' ' + dict_response['last_name']

            genders = ['', 'female', 'male']
            # Ответ приходит в формате 0/1/2,поэтому адресуемся по индексу массива
            dict_response['gender'] = genders[dict_response['sex']]

        dict_response['username'] = dict_response['name']
        dict_response['name'] = social.agent + str(dict_response['uid'])

        # Принадлежность к определеной соцсети
        dict_response['via'] = social.agent
        # Унификация гендерного признака сведением к одной буквок F(female) или M(male)
        dict_response['gender'] = dict_response['gender'][0]

        return dict_response  # Отдаем обратно унифицированый ответ

    @staticmethod
    def get_user(params):
        via = params['via']
        uid = params['uid']
        if SocialUser.user_exist(via, uid):
            # Если он уже существует у нас в базе - мы соответственно не будем его создавать заново,
            # но мы можем найти профиль связаный с ним в этом случае
            user = SocialUser.objects.get(via=via, uid=uid).user
        else:
            # Иначе нам следует его добавить в нашу базу и завязать на нем новый профиль
            # Заморочка в том что у нас стоит проверка на сложность пароля и на его наличие.
            # Ну и валидность/различность email-ов.Посему придется имитировать каждый раз новые пароли и адреса почты
            email = get_random_string(5) + '@' + get_random_string(5) + '.' + get_random_string(3)
            password = get_random_string(6)  # Пароль сохранится в корявом виде,но это не страшно
            user = Account.objects.create_user(params['name'], email, password)
            # Т.к у нас Account частично перекрывает стандартный User -
            # мы не можем в лоб сохранить некоторые параметры
            user.first_name = params['first_name']
            user.last_name = params['last_name']
            user.birth_day = params['birthday']
            user.gender = params['gender']
            # Т.к. все вновь приходящие будут простыми пользователями на данном этапе мы им ничего не даём
            user.save()

            # Дополнительно запишем в расширеную таблицу сведений все важные данные
            SocialUser.create(params, user)

        return user
