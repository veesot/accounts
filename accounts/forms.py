from django import forms


class AccountForm(forms.Form):

    last_name = forms.CharField(label='Фамилия', max_length=100)
    first_name = forms.CharField(label='Имя', max_length=100)
    middle_name = forms.CharField(label='Отчество', max_length=100,required=False)
    email = forms.CharField(label='Эл.почта', required=False)
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(),required=False)

    class Meta:
        exclude = ('last_name',)
