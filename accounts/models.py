import datetime
from django.contrib.auth.models import UserManager, User, Group
from django.contrib.auth.views import logout
from django.db import models
from django.db.models import CharField, ForeignKey, DateField
from service.service import get_random_string


class Account(User):
    birth_day = DateField(default=datetime.date.today)
    middle_name = CharField(max_length=30, blank=True)
    gender = CharField(max_length=1, blank=True)
    passport_number = CharField(max_length=10, blank=True, verbose_name="Номер паспорта")
    registration = CharField(max_length=200, blank=True, verbose_name="Прописка")
    objects = UserManager()

    class Meta:
        db_table = 'users'
        verbose_name = 'Пользователь'
        verbose_name_plural = "Пользователи"

    def get_short_name(self):
        super().get_short_name()

    def get_full_name(self):
        super().get_full_name()

    @staticmethod
    def login(request, user):
        request.user = user
        return request

    @staticmethod
    def log_out(request):
        logout(request)

    @staticmethod
    def get_all_users():
        return Account.objects.all()


class SocialUser(models.Model):
    """Информация от социалок и прочих стороних источников"""
    user = ForeignKey(Account)
    name = CharField(verbose_name="Имя в сети", max_length=72)
    via = CharField(verbose_name="Провайдер", max_length=5)
    uid = CharField(verbose_name="Идентификатор в сети", max_length=20)

    @staticmethod
    def create(user_params, user):
        SocialUser.objects.create(via=user_params['via'], uid=user_params['uid'],
                                  name=user_params['username'], user=user)

    @classmethod
    def user_exist(cls, via, uid):
        return SocialUser.objects.filter(via=via, uid=uid).exists()

    @classmethod
    def get_social_user(cls, params):
        via = params['via']
        uid = params['uid']

        if cls.user_exist(via, uid):
            # Если он уже существует у нас в базе - мы соответственно не будем его создавать заново,
            # но мы можем найти профиль связаный с ним в этом случае
            user = cls.objects.get(via=via, uid=uid).user
        else:
            # Иначе нам следует его добавить в нашу базу и завязать на нем новый профиль
            # Заморочка в том что у нас стоит проверка на сложность пароля и на его наличие.
            # Ну и валидность/различность email-ов.Посему придется имитировать каждый раз новые пароли и адреса почты
            email = get_random_string(5) + '@' + get_random_string(5) + '.' + get_random_string(3)
            password = get_random_string(6)  # Пароль сохранится в корявом виде,но это не страшно
            user = Account.objects.create_user(params['name'], email, password)
            # Т.к у нас Account частично перекрывает стандартный User -
            # мы не можем в лоб сохранить некоторые параметры
            user.birth_day = params['birthday']
            user.gender = params['gender']
            user.save()

            # Дополнительно запишем в расширеную таблицу сведений все важные данные
            cls.create(params, user)
        return user

    class Meta:
        db_table = 'social_users'

