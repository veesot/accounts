"""Module for auth users"""
from django.apps import apps
from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.core.exceptions import ImproperlyConfigured

try:
    get_model = apps.get_model
except ImportError:
    from django.db.models.loading import get_model


class CustomUserModelBackend(ModelBackend):
    def __init__(self):
        self._user_class = get_model(*settings.CUSTOM_USER_MODEL.split('.', 2))

    @property
    def user_class(self):
        if not hasattr(self, '_user_class'):
            if not self._user_class:
                raise ImproperlyConfigured('Could not get custom user model')
        return self._user_class

    def authenticate(self, username=None, password=None, **kwargs):
        try:
            user = self.user_class.objects.get(username=username)
            if user.check_password(password):
                return user
        except self.user_class.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return self.user_class.objects.get(pk=user_id)
        except self.user_class.DoesNotExist:
            return None
