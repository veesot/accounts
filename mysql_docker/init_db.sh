#!/usr/bin/env bash
/usr/bin/mysqld_safe &
sleep 5
mysql -u root -ptoor -e "CREATE DATABASE spark"
mysql -u root -ptoor spark < /tmp/dump.sql